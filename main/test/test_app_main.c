#include "unity.h"
#include "fff.h"

#include "sdkconfig.h"
#include "esp_log.h"
#include "mock_button.h"

TEST_FILE("log.c")
TEST_FILE("app_main.c")

void app_main(void);

void setUp(void)
{
  
}

void tearDown(void)
{
  
}

void test_app_main_ShouldInitConfiguredGPIO(void)
{
  app_main();
  TEST_ASSERT_EQUAL_UINT(1, button_init_fake.call_count);
  TEST_ASSERT_TRUE(CONFIG_BUTTON_GPIO == button_init_fake.arg0_val);
}
