#include <stdlib.h>
#include <string.h>

#include "unity.h"
#include "fff.h"

#include "esp_log.h"
#include "mock_gpio.h"
#include "button.h"

TEST_FILE("log.c")

#define GPIO_CONFIG_CLEAR_LAST_CONFIG() \
  memset(&gpio_config_last_config, 0, sizeof(gpio_config_last_config))

gpio_config_t gpio_config_last_config = {0};
  
esp_err_t gpio_config_stub(const gpio_config_t *pGPIOConfig)
{
  if (NULL != pGPIOConfig) {
    memcpy(&gpio_config_last_config, pGPIOConfig, sizeof(gpio_config_last_config));
    return ESP_OK;
  }
  else {
    return ESP_ERR_INVALID_ARG;
  }
}

void setUp(void)
{
  GPIO_CONFIG_CLEAR_LAST_CONFIG();
  gpio_config_fake.custom_fake = gpio_config_stub;
}

void tearDown(void)
{
  
}

void test_button_init_ShouldConfigGPIOAsInput(void)
{
  const gpio_num_t test_gpio = 4;
  button_err_t err = 0;
  
  err = button_init(test_gpio);
  
  TEST_ASSERT_EQUAL_INT(Button_ok, err);
  TEST_ASSERT_EQUAL_INT(1, gpio_config_fake.call_count);
  TEST_ASSERT_EQUAL_INT(ESP_OK, gpio_config_fake.return_val_history[0]);
  TEST_ASSERT_EQUAL_HEX64(1ULL << test_gpio, gpio_config_last_config.pin_bit_mask);
  TEST_ASSERT_EQUAL_INT(GPIO_MODE_INPUT, gpio_config_last_config.mode);
}

void test_button_init_ShouldEnableFallingEdgeInterruptsOnGPIO(void)
{
  const gpio_num_t test_gpio = 4;
  button_err_t err = 0;
  
  err = button_init(test_gpio);
  
  TEST_ASSERT_EQUAL_INT(Button_ok, err);
  TEST_ASSERT_EQUAL_INT(1, gpio_config_fake.call_count);
  TEST_ASSERT_EQUAL_INT(ESP_OK, gpio_config_fake.return_val_history[0]);
  TEST_ASSERT_EQUAL_HEX64(1ULL << test_gpio, gpio_config_last_config.pin_bit_mask);
  TEST_ASSERT_EQUAL_INT(GPIO_INTR_NEGEDGE, gpio_config_last_config.intr_type);
}

void test_button_init_ShouldReportErrorWhenConfigGPIOFails(void)
{
  const gpio_num_t test_gpio = 4;
  button_err_t err = 0;
  
  gpio_config_fake.custom_fake = NULL;
  gpio_config_fake.return_val = ESP_FAIL;
  
  err = button_init(test_gpio);
  
  TEST_ASSERT_EQUAL_INT(Button_error, err);
}
