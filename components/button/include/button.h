#ifndef BUTTON_H
#define BUTTON_H

#include "driver/gpio.h"

typedef enum button_err_enum {
	Button_error = -1,
	Button_ok = 0
} button_err_t;

/**
 * @brief Setups given GPIO as input with falling edge interrupts.
 * 
 * @param butt_gpio GPIO number where button is connected.
 */
button_err_t button_init(gpio_num_t butt_gpio);

#endif // BUTTON_H
