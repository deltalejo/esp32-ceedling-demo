Work in progress...

# ESP32 Ceedling demo
Sample project to show how to use Ceedling for unit testing apps developed with ESP-IDF.

## Requirements
- [ESP-IDF v4.0](https://docs.espressif.com/projects/esp-idf/en/v4.0/get-started)
- [Ceedling](https://www.throwtheswitch.org/ceedling)
- ESP32 based board to have fun with
