#include <stdint.h>
#include <stdarg.h>
#include <stdio.h>
#include <time.h>

#include "esp_log.h"

uint32_t esp_log_timestamp(void)
{
    return time(NULL);
}

void esp_log_write(esp_log_level_t level,
        const char* tag,
        const char* format, ...)
{
    va_list list;
    va_start(list, format);
    vprintf(format, list);
    va_end(list);
}
